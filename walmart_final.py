
# coding: utf-8

# In[1]:

import pandas as pd
import numpy as np
from numpy import *

import gc
import time
import warnings
import os

from datetime import datetime
from datetime import timedelta

from scipy.misc import imread
from scipy import sparse
from scipy.stats import ss
import math

import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import plotly
import plotly.offline as py
import plotly.tools as tls
import plotly.graph_objs as go

from sklearn.ensemble import RandomForestRegressor
from sklearn.utils.validation import check_X_y,check_is_fitted
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
from sklearn.metrics import log_loss
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix,accuracy_score
from scipy import sparse

import keras
from keras.layers.core import Dense,Activation,Dropout
from keras.layers.recurrent import LSTM
from keras.layers import Bidirectional
from keras.models import Sequential
from keras import regularizers
from keras import optimizers

from sklearn.metrics import mean_squared_error




# In[2]:

print("Reading Data....")
print(os.listdir('.'))


# In[ ]:

train = pd.read_csv('train.csv')


# In[4]:

print(len(train))


# In[5]:

train.tail()


# In[ ]:

featuers_df = pd.read_csv('features.csv')


# In[7]:

featuers_df.columns


# In[8]:

featuers_df.shape


# In[9]:

featuers_df.head()


# In[ ]:

stores_df = pd.read_csv('stores.csv')


# In[11]:

stores_df.columns


# In[12]:

stores_df.shape


# In[13]:

stores_df.head()


# In[ ]:

df1 = pd.merge(stores_df,train,on='Store')


# In[15]:

df1.shape


# In[16]:

df1.columns


# In[ ]:

df = pd.merge(df1,featuers_df,on=['Store','Date'])


# In[18]:

df.tail()


# In[19]:

df.describe().T


# In[ ]:

df['Temperature'] = (df['Temperature'] - 32) * 5/9


# In[ ]:

sns.set(style='white')


# In[ ]:

corr = df.corr()


# In[ ]:

mask = np.zeros_like(corr,dtype=np.bool)
mask[np.triu_indices_from(mask)] = True


# In[24]:

f,ax = plt.subplots(figsize=(11,9))
cmap = sns.diverging_palette(220,10,as_cmap=True)


# In[25]:

sns.heatmap(corr,mask=mask,cmap=cmap,vmax=.3,center=0,square=True,linewidths=0.5,cbar_kws={"shrink":0.5})


# In[26]:

df.loc[df['Weekly_Sales'] > 300000]


# In[27]:

df.loc[df['Weekly_Sales']>240000,'Date'].value_counts()


# In[28]:

df.isnull().sum()


# In[ ]:

df = df.assign(md1_present = df.MarkDown1.notnull())


# In[ ]:

df = df.assign(md2_present = df.MarkDown2.notnull())
df = df.assign(md3_present = df.MarkDown3.notnull())
df = df.assign(md4_present = df.MarkDown4.notnull())
df = df.assign(md5_present = df.MarkDown5.notnull())


# In[ ]:

df.fillna(0,inplace=True)


# In[ ]:

df['Type'] = 'Type_' + df['Type'].map(str)


# In[ ]:

df = df.drop(['IsHoliday_y'],axis=1)


# In[ ]:

df['Store'] = 'Store_' + df['Store'].map(str)
df['Dept'] = 'Dept_' + df['Dept'].map(str)
df['IsHoliday_x'] = 'IsHoliday_' + df['IsHoliday_x'].map(str)


# In[ ]:

type_dummies = pd.get_dummies(df['Type'])
store_dummies = pd.get_dummies(df['Store'])
dept_dummies = pd.get_dummies(df['Dept'])
holiday_dummies = pd.get_dummies(df['IsHoliday_x'])


# In[ ]:

df['DateType'] = [datetime.strptime(date, '%Y-%m-%d').date() for date in df['Date'].astype(str).values.tolist()]
df['Month'] = [date.month for date in df['DateType']]
df['Month'] = 'Month_' + df['Month'].map(str)
Month_dummies = pd.get_dummies(df['Month'] )


# In[ ]:

df['Black_Friday'] = np.where((df['DateType']==datetime(2010, 11, 26).date()) | (df['DateType']==datetime(2011, 11, 25).date()), 'yes', 'no')
df['Pre_christmas'] = np.where((df['DateType']==datetime(2010, 12, 23).date()) | (df['DateType']==datetime(2010, 12, 24).date()) | (df['DateType']==datetime(2011, 12, 23).date()) | (df['DateType']==datetime(2011, 12, 24).date()), 'yes', 'no')
df['Black_Friday'] = 'Black_Friday_' + df['Black_Friday'].map(str)
df['Pre_christmas'] = 'Pre_christmas_' + df['Pre_christmas'].map(str)
Black_Friday_dummies = pd.get_dummies(df['Black_Friday'] )
Pre_christmas_dummies = pd.get_dummies(df['Pre_christmas'] )


# In[ ]:

df = pd.concat([df,holiday_dummies,Pre_christmas_dummies,Black_Friday_dummies],axis=1)


# In[39]:

medians = pd.DataFrame({'Median Sales' :df.groupby(by=['Type','Dept','Store','Month','IsHoliday_x'])['Weekly_Sales'].median()}).reset_index()
medians.head()


# In[ ]:

df = df.merge(medians, how = 'outer', on = ['Type','Dept','Store','Month','IsHoliday_x'])


# In[ ]:

# Fill NA
df['Median Sales'].fillna(df['Median Sales'].median(), inplace=True) 

# Create a key for easy access

df['Key'] = df['Type'].map(str)+df['Dept'].map(str)+df['Store'].map(str)+df['Date'].map(str)+df['IsHoliday_x'].map(str)


# In[42]:

df.head()


# In[43]:

df['DateLagged'] = df['DateType']- timedelta(days=7)
df.head()


# In[ ]:

sorted_df = df.sort_values(['Store', 'Dept','DateType'], ascending=[1, 1,1])
sorted_df = sorted_df.reset_index(drop=True) # Reinitialize the row indices for the loop to work


# In[45]:

sorted_df['LaggedSales'] = np.nan # Initialize column
sorted_df['LaggedAvailable'] = np.nan # Initialize column
last=df.loc[0] # intialize last row for first iteration. Doesn't really matter what it is
row_len = sorted_df.shape[0]
for index, row in sorted_df.iterrows():
    lag_date = row["DateLagged"]
    # Check if it matches by comparing last weeks value to the compared date 
    # And if weekly sales aren't 0
    if((last['DateType']== lag_date) & (last['Weekly_Sales']>0)): 
        sorted_df.set_value(index, 'LaggedSales',last['Weekly_Sales'])
        sorted_df.set_value(index, 'LaggedAvailable',1)
    else:
        sorted_df.set_value(index, 'LaggedSales',row['Median Sales']) # Fill with median
        sorted_df.set_value(index, 'LaggedAvailable',0)

    last = row #Remember last row for speed
    if(index%int(row_len/10)==0): #See progress by printing every 10% interval
        print(str(int(index*100/row_len))+'% loaded')


# In[46]:

sorted_df[['Dept', 'Store','DateType','LaggedSales','Weekly_Sales','Median Sales']].head()


# In[ ]:

df = df.merge(sorted_df[['Dept', 'Store','DateType','LaggedSales','LaggedAvailable']], 
              how = 'inner', on = ['Dept', 'Store','DateType'])


# In[48]:

df['Sales_dif'] = df['Median Sales'] - df['LaggedSales']
df[['Dept', 'Store','DateType','LaggedSales','Weekly_Sales','Median Sales']].head()


# In[ ]:

df['Difference'] = df['Median Sales'] - df['Weekly_Sales']


# In[ ]:

switch= 1

if(switch):
    df_backup = df.copy()
else:
    df=df_backup
    display(df_backup.head())


# In[ ]:

df['Unemployment'] = (df['Unemployment'] - df['Unemployment'].mean())/(df['Unemployment'].std())
df['Temperature'] = (df['Temperature'] - df['Temperature'].mean())/(df['Temperature'].std())
df['Fuel_Price'] = (df['Fuel_Price'] - df['Fuel_Price'].mean())/(df['Fuel_Price'].std())
df['CPI'] = (df['CPI'] - df['CPI'].mean())/(df['CPI'].std())
df['MarkDown1'] = (df['MarkDown1'] - df['MarkDown1'].mean())/(df['MarkDown1'].std())
df['MarkDown2'] = (df['MarkDown2'] - df['MarkDown2'].mean())/(df['MarkDown2'].std())
df['MarkDown3'] = (df['MarkDown3'] - df['MarkDown3'].mean())/(df['MarkDown3'].std())
df['MarkDown4'] = (df['MarkDown4'] - df['MarkDown4'].mean())/(df['MarkDown4'].std())
df['MarkDown5'] = (df['MarkDown5'] - df['MarkDown5'].mean())/(df['MarkDown5'].std())
df['LaggedSales']= (df['LaggedSales'] - df['LaggedSales'].mean())/(df['LaggedSales'].std())
df['Weekly_Sales']= (df['Weekly_Sales'] - df['Weekly_Sales'].mean())/(df['Weekly_Sales'].std())
df['Median Sales']= (df['Median Sales'] - df['Median Sales'].mean())/(df['Median Sales'].std())
df['Sales_dif']= (df['Sales_dif'] - df['Sales_dif'].mean())/(df['Sales_dif'].std())
df['Difference']= (df['Difference'] - df['Difference'].mean())/(df['Difference'].std())
df['Size']= (df['Size'] - df['Size'].mean())/(df['Size'].std())


# In[52]:

df_backup.head()


# In[51]:

# Code from https://seaborn.pydata.org/examples/many_pairwise_correlations.html
sns.set(style="white")

# Compute the correlation matrix
corr = df.corr()

# Generate a mask for the upper triangle
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(220, 10, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.5, cbar_kws={"shrink": .5})


# In[ ]:

selector = [
    #'Month',
    'CPI',
    'Fuel_Price',
    'MarkDown1',
    'MarkDown2',
    'MarkDown3',
    'MarkDown4',
    'MarkDown5',
    'Size',
    'Temperature',
    'Unemployment',
    
    
    
    'md1_present',
    'md2_present',
    'md3_present',
    'md4_present',
    'md5_present',

    'IsHoliday_False',
    'IsHoliday_True',
    'Pre_christmas_no',
    'Pre_christmas_yes',
    'Black_Friday_no',
    'Black_Friday_yes',    
    'Difference',
    'Sales_dif',
    'LaggedAvailable',    
    'LaggedSales'
    ]
#display(df[selector].describe())
#display(df[selector].head())
#display(df[['DateType','DateLagged','LaggedSales','Weekly_Sales']])


# In[53]:

len(selector)


# In[ ]:

np.random.seed(42)
#X_train, X_dev, y_train, y_dev = train_test_split(df[selector], df['Weekly_Sales'], test_size=0.2, random_state=42)
#print(X_dev.shape)
#print(y_dev.shape)


# In[ ]:

from sklearn.model_selection import KFold


# In[ ]:

scores = []
#x_train = df[selector]
#y_train = df['Weekly_Sales']


# In[ ]:

#scaledFeatures = ['Unemployment','Temperature','Fuel_Price','CPI','MarkDown1','MarkDown2',
#                 'MarkDown3','MarkDown4','MarkDown5','LaggedSales','Weekly_Sales','Median Sales',
#               'Sales_dif','Difference','Size']
#from sklearn import preprocessing
#scaler = preprocessing.StandardScaler().fit(df[scaledFeatures])
#df[scaledFeatures] = scaler.transform(df[scaledFeatures])


# In[62]:

df.head()


# In[ ]:

kfold = KFold(n_splits=5,random_state=1).split(df)


# In[60]:

scores = []
predicted_weeky_sales_avg = []
predicted_weeky_sales = pd.DataFrame({"Weekly_Sales": []})
actual_test = []
for k,(train,test) in enumerate(kfold):
  predicted_weeky_sales = []
  X_train = df.iloc[train][selector]
  y_train = df.iloc[train]['Weekly_Sales']
  X_dev = df.iloc[test][selector]
  y_dev = df.iloc[test]['Weekly_Sales']
  adam_regularized = Sequential()

  # First hidden layer now regularized
  train_X = X_train.values.reshape((X_train.shape[0], 1, X_train.shape[1]))
  test_X = X_dev.values.reshape((X_dev.shape[0], 1, X_dev.shape[1]))
  
  adam_regularized.add(LSTM(25,input_shape=(1,X_train.shape[1]),activation='relu',
                    kernel_regularizer = regularizers.l2(0.01),
                             return_sequences=True))
  adam_regularized.add(Dropout(0.2))
    
    # Second hidden layer now regularized
  adam_regularized.add(LSTM(16,activation='relu',
                       kernel_regularizer = regularizers.l2(0.01)
                              ,return_sequences=False))
  adam_regularized.add(Dropout(0.2))
  
  adam_regularized.add(Dense(1,activation='linear'))

  # Setup adam optimizer
  adam_optimizer=keras.optimizers.Adam(lr=0.01,
                  beta_1=0.9, 
                  beta_2=0.999, 
                  epsilon=1e-08)

  # Compile the model
  adam_regularized.compile(optimizer=adam_optimizer,
                loss='mean_absolute_error',
                metrics=['mae'])

  # Train
  history_adam_regularized=adam_regularized.fit(train_X, y_train, # Train on training set
                               epochs=10, # We will train over 1,000 epochs
                               batch_size=2048, # Batch size 
                               verbose=0) # Suppress Keras output
  score = adam_regularized.evaluate(x=test_X,y=y_dev)
  scores.append(score[0])
  print('Fold: %2d, mae: %.3f' %(k+1,score[0]))
  
  predictions = adam_regularized.predict(x=test_X)
  actual_test.append(df_backup.iloc[test]['Weekly_Sales'])
  
  predicted_weeky_sales_avg.append(predictions)
print("%.2f%% (+/- %.2f%%)" % (np.mean(scores), np.std(scores)))


# In[ ]:

predicted_weeky_sales = (predicted * df_backup['Weekly_Sales'].std()) + df_backup['Weekly_Sales'].mean()


# In[87]:

predictedAvg = []
itr = zip(predicted_weeky_sales_avg, actual_test)
for values in itr:
  actual_test0 = values[1].reshape((84314,))
  predicted = values[0].reshape((84314,))
  predict_actuals_diff = actual_test0 - predicted
  predictedAvg.append(np.abs(np.sum(predict_actuals_diff))/84314)
  print(np.abs(np.sum(predict_actuals_diff))/84314)
print(np.sum(predictedAvg)/5)


# In[127]:

scores = []
neural = True
for i in range(10):
    # Sequential model
    X_train, X_dev, y_train, y_dev = train_test_split(df[selector], df['Weekly_Sales'], test_size=0.2, random_state=42)
    
    train_X = X_train.values.reshape((X_train.shape[0], 1, X_train.shape[1]))
    test_X = X_dev.values.reshape((X_dev.shape[0], 1, X_dev.shape[1]))
    
    adam_regularized = Sequential()

    # First hidden layer now regularized
    adam_regularized.add(LSTM(32,input_shape=(1,X_train.shape[1]),activation='relu',
                    kernel_regularizer = regularizers.l2(0.01),
                             return_sequences=True))
    adam_regularized.add(Dropout(0.2))
    
    # Second hidden layer now regularized
    adam_regularized.add(LSTM(16,activation='relu',
                       kernel_regularizer = regularizers.l2(0.01)
                              ,return_sequences=False))
    adam_regularized.add(Dropout(0.2))

    # Output layer stayed sigmoid
    adam_regularized.add(Dense(1,activation='linear'))

    # Setup adam optimizer
    adam_optimizer=keras.optimizers.Adam(lr=0.01,
                    beta_1=0.9, 
                    beta_2=0.999, 
                    epsilon=1e-08)

    # Compile the model
    adam_regularized.compile(optimizer=adam_optimizer,
                  loss='mean_absolute_error',
                  metrics=['mse'])

    # Train
    history_adam_regularized=adam_regularized.fit(train_X, y_train, # Train on training set
                                 epochs=10, # We will train over 1,000 epochs
                                 batch_size=2048, # Batch size 
                                 verbose=0) # Suppress Keras output
    score = adam_regularized.evaluate(x=test_X,y=y_dev)
    scores.append(score[0])

    # Plot network
    plt.plot(history_adam_regularized.history['loss'], label='Adam Regularized')
    plt.xlabel('Epochs')
    plt.ylabel('loss')
    plt.legend()
    y_pred_neural = adam_regularized.predict(test_X)
plt.show()


# In[ ]:

X_train, X_dev, y_train, y_dev = train_test_split(df[selector], df['Weekly_Sales'], test_size=0.2, random_state=42)
regr = RandomForestRegressor(n_estimators=100, criterion='mae', max_depth=None, 
                      min_samples_split=2, min_samples_leaf=1, 
                      min_weight_fraction_leaf=0.0, max_features=5, 
                      max_leaf_nodes=None, min_impurity_decrease=0.0, 
                      min_impurity_split=None, bootstrap=True, 
                      oob_score=False, n_jobs=1, random_state=None, 
                      verbose=2, warm_start=False)

#Train on data
regr.fit(X_train, y_train.ravel())


# In[58]:

y_pred_random = regr.predict(X_dev)

y_dev = y_dev.to_frame()


# In[63]:

print(regr.score(X_dev,y_dev['Weekly_Sales']))


# In[ ]:

y_dev = y_dev.to_frame()


# In[59]:

# Transform forest predictions to observe direction of change

y_dev['Predicted'] = y_pred_random
#y_dev['Predicted'] = y_pred_neural
#direction_true1= binary(y_dev.values)
#direction_predict = binary(y_pred_random)

direction_true1 = y_dev.Weekly_Sales.apply(lambda x: 1 if x > 0 else -1)
direction_predict = y_dev.Predicted.apply(lambda x: 1 if x > 0 else -1)
#direction_predict = np.apply_along_axis(lambda x: 1 if x > 0 else -1, 0, y_pred_random)

## show confusion matrix random forest
cnf_matrix = confusion_matrix(direction_true1, direction_predict)

fig, ax = plt.subplots(1)
ax = sns.heatmap(cnf_matrix, ax=ax, cmap=plt.cm.Greens, annot=True)
#ax.set_xticklabels(abbreviation)
#ax.set_yticklabels(abbreviation)
plt.title('Confusion matrix of random forest predictions')
plt.ylabel('True category')
plt.xlabel('Predicted category')
plt.show();

